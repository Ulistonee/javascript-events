const { merge } = require('webpack-merge');
const TerserPlugin = require('terser-webpack-plugin');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'production',
  // entry: './entry.js',
  // devtool: 'source-map',
  // output: {
  //   path: './dist',
  //   filename: PROD ? 'bundle.min.js' : 'bundle.js'
  // },
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({ parallel: true }),
    ],
  },
});
