export function createSection(titleName, buttonName) {
  const joinOurProgram = document.createElement('section');
  joinOurProgram.classList.add('app-section');
  joinOurProgram.classList.add('app-section--image-join-our-program');
  /*eslint-disable */
  const title = document.createElement("h2");

  /* eslint-enable */
  title.textContent = titleName;
  title.classList.add('app-title');

  const subtitle = document.createElement('h3');
  subtitle.innerHTML = 'Sed do eiusmod tempor incididunt <br>ut labore et dolore magna aliqua.';
  subtitle.classList.add('app-subtitle');

  const form = document.createElement('form');
  form.setAttribute('method', 'post');
  form.classList.add('app-section__form');

  const input = document.createElement('input');
  input.setAttribute('type', 'email');
  input.setAttribute('placeholder', 'Email');
  input.setAttribute('name', 'input');
  input.classList.add('app-section__input');

  const submitButton = document.createElement('button');
  submitButton.textContent = buttonName;
  submitButton.classList.add('app-section__button');
  submitButton.classList.add('app-section__button--join-our-program');

  form.append(input);
  form.append(submitButton);

  joinOurProgram.append(title);
  joinOurProgram.append(subtitle);
  joinOurProgram.append(form);
  return joinOurProgram;
}

function createCommunitySection() {
  const section = document.createElement('section');
  section.classList.add('app-section');
  section.classList.add('app-section--community');

  const title = document.createElement('h2');

  title.textContent = 'Big Community Of People Like You';
  title.classList.add('app-title');

  const subtitle = document.createElement('h3');
  subtitle.innerHTML = 'We’re proud of our products, and we’re really excited <br> when we get feedback from our users.';
  subtitle.classList.add('app-subtitle');

  section.append(title);
  section.append(subtitle);
  return section;
}

export function sectionFactory(type) {
  switch (type) {
    case 'standard':
      return createSection('Join Our Program', 'Subscribe');
    case 'advanced':
      return createSection('Join Our Advanced Program', 'Subscribe Advanced Program');
    case 'community':
      return createCommunitySection();
    default:
      throw new Error('Wrong type of section');
  }
}
