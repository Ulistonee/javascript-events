module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: 'airbnb-base',
  overrides: [
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  rules: {
    semi: [ 'warn', 'always' ],
    quotes: [ 2, 'single' ],
    'array-bracket-spacing': [ 1, 'always' ],
    'array-element-newline': [ 1, 'never' ],
    'arrow-spacing': [ 1, { before: true, after: true } ],
    'no-unused-vars': 'error',
    'no-dupe-else-if': 'error',
    'no-irregular-whitespace': 'error',
    'no-undef': 'error',
    'no-unreachable-loop': 'error',
  },
};
