import { expect } from 'chai';
import sinon, { stub } from 'sinon';
import {
  validate, validateWithLog, validateAsync, validateWithThrow,
} from '../scripts/email-validator.js';

describe('validate - basic functionality', () => {
  it('should return false when passed email which does not end with "gmail.com"', () => {
    const wrongEmail = 'test@bk.ru';
    const actual = validate(wrongEmail);
    expect(false).to.equal(actual);
  });
  it('should return false when passed email which does not end with "outlook.com"', () => {
    const wrongEmail = 'outlook.com@bk.ru';
    const actual = validate(wrongEmail);
    expect(false).to.equal(actual);
  });
  it('should return false when passed only domen "outlook.com"', () => {
    const wrongEmail = 'outlook.com';
    const actual = validate(wrongEmail);
    expect(false).to.equal(actual);
  });
  it('should return false when passed only domen "gmail.com"', () => {
    const wrongEmail = 'gmail.com';
    const actual = validate(wrongEmail);
    expect(false).to.equal(actual);
  });
  it('should return false when passed only domen "@outlook.com"', () => {
    const wrongEmail = '@outlook.com';
    const actual = validate(wrongEmail);
    expect(false).to.equal(actual);
  });
  it('should return false when passed only domen "@gmail.com"', () => {
    const wrongEmail = '@gmail.com';
    const actual = validate(wrongEmail);
    expect(false).to.equal(actual);
  });
});

describe('validateAsync - basic functionality', () => {
  it('should return false when passed email which does not end with "gmail.com"', async () => {
    const wrongEmail = 'test@bk.ru';
    const actual = await validateAsync(wrongEmail);
    expect(false).to.equal(actual);
  });
  it('should return false when passed email which only contains "@gmail.com"', async () => {
    const wrongEmail = '@gmail.com';
    const actual = await validateAsync(wrongEmail);
    expect(false).to.equal(actual);
  });
});

describe('validateWithThrow - basic functionality', () => {
  it('should return throw Error when passed email which does not end with "gmail.com"', () => {
    const wrongEmail = 'test@bk.ru';
    expect(validateWithThrow.bind(validateWithThrow, wrongEmail)).to.throw(Error, /provided email is not valid/);
  });
  it('should return throw Error when passed email which only contains "@gmail.com"', () => {
    const wrongEmail = '@gmail.com';
    expect(validateWithThrow.bind(validateWithThrow, wrongEmail)).to.throw(Error, /provided email is not valid/);
  });
  it('should return true when passed email which ends with "gmail.com"', () => {
    const email = 'aizhan@gmail.com';
    const actual = validateWithThrow(email);
    expect(true).to.equal(actual);
  });
});

describe('validateWithLog - basic functionality', () => {
  it('should return false when passed email which does not end with "gmail.com"', () => {
    const stub = sinon.stub(console, 'log')
      .resolves(null);
    const wrongEmail = 'test@bk.ru';
    const actual = validateWithLog(wrongEmail);
    expect(false).to.equal(actual);
    stub.restore();
  });
  it('should return false when passed email which does not end with "gmail.com"', () => {
    const wrongEmail = '@gmail.com';
    const actual = validateWithLog(wrongEmail);
    expect(false).to.equal(actual);
  });

});
