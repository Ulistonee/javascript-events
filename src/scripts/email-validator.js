export const VALID_EMAIL_ENDINGS = [ '@gmail.com', '@outlook.com', '@mail.ru' ];

export function validate(email) {
  if (email !== '@gmail.com' && email !== '@outlook.com') {
    return email.endsWith(VALID_EMAIL_ENDINGS[0]) || email.endsWith(VALID_EMAIL_ENDINGS[1]);
  }
  return false;
}

export function validateAsync(email) {
  return new Promise((res) => {
    if (email !== '@gmail.com' && email !== '@outlook.com') {
      res(email.endsWith(VALID_EMAIL_ENDINGS[0]) || email.endsWith(VALID_EMAIL_ENDINGS[1]));
    }
    res(false);
  });
}

export function validateWithThrow(email) {
  if (email !== '@gmail.com' && email !== '@outlook.com') {
    const res = email.endsWith(VALID_EMAIL_ENDINGS[0]) || email.endsWith(VALID_EMAIL_ENDINGS[1]);
    if (res === false) {
      throw new Error('provided email is not valid');
    }
    return true;
  }
  throw new Error('provided email is not valid');
}

export function validateWithLog(email) {
  if (email !== '@gmail.com' && email !== '@outlook.com') {
    console.log(email.endsWith(VALID_EMAIL_ENDINGS[0]) || email.endsWith(VALID_EMAIL_ENDINGS[1]));
    return email.endsWith(VALID_EMAIL_ENDINGS[0]) || email.endsWith(VALID_EMAIL_ENDINGS[1]);
  }
  console.log(false);
  return false;
}
