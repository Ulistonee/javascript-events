const { merge } = require('webpack-merge');
const path = require('path');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'development',
  devServer: {
        static: {
          directory: path.join(__dirname, 'dist'),
        },
        hot: true,
        port: 8080,
        proxy: {
          '/api': 'http://localhost:8080',
        },
          headers: {
            "Cross-Origin-Embedder-Policy": "require-corp",
            "Cross-Origin-Opener-Policy": "same-origin"
        }
    },
});
