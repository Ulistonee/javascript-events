import { sectionFactory } from './join-us-section.js';
import { validate } from './email-validator.js';
import './css/style.css';
import { makeHidden } from './utils.js';
import { makeGetRequest, subscribe, unsubscribe } from './query.js';
import { logPerformance } from "./utils.js";

function makeSubscriptionSection(type) {
  const section = sectionFactory(type);
  const form = section.querySelector('.app-section__form');
  const input = section.querySelector('.app-section__input');
  const button = section.querySelector('.app-section__button--join-our-program');

  form.addEventListener('submit', (e) => {
    e.preventDefault();
    if (localStorage.getItem('subscribed') === 'yes') {
      unsubscribe('http://localhost:3000/unsubscribe', button, input);
    } else if (validate(input.value)) {
      subscribe(input, 'http://localhost:3000/subscribe', button);
    }
  });

  if (localStorage.getItem('subscribed') === 'yes') {
    makeHidden(input, button);
  } else {
    input.onchange = function () {
      localStorage.setItem('email', input.value);
    };
    input.value = localStorage.getItem('email');
  }
  return section;
}

async function makeCommunitySection() {
  const section = sectionFactory('community');
  const data = await makeGetRequest('http://localhost:3000/community');

  const gallery = document.createElement('ul');
  for (const person of data) {
    const card = document.createElement('li');
    const avatar = document.createElement('img');
    avatar.src = person.avatar;
    avatar.alt = 'avatar';
    avatar.crossOrigin = "anonymous";
    const title = document.createElement('p');
    title.textContent = person.phrase;
    const name = document.createElement('p');
    name.textContent = `${person.firstName} ${person.lastName}`;
    name.classList.add('app-gallery__name');
    const position = document.createElement('p');
    position.textContent = person.position;

    card.append(avatar);
    card.append(title);
    card.append(name);
    card.append(position);
    gallery.append(card);
  }
  gallery.classList.add('app-gallery');
  section.append(gallery);
  return section;
}

window.addEventListener('load', async () => {
    const appFooter = document.querySelector('.app-footer');

    appFooter.before(makeSubscriptionSection('standard'));
    appFooter.before(await makeCommunitySection());
    await logPerformance();
});

let count = 0;
const worker = new Worker(new URL('./worker.js', import.meta.url));
window.addEventListener('click', () => {
    count++;

    if (count === 5){
        worker.postMessage({
            action: "send"
        })
        count = 0;
    }
})
