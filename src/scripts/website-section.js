class WebsiteSection extends HTMLElement {
  constructor() {
    super();

    this.attachShadow({ mode: 'open' });
    this.shadowRoot.innerHTML = `
        <link rel="stylesheet" href="./index.css">
        <section class="app-section">
              <slot name="logo"></slot>
              <slot name="title"></slot>
              <slot name="button"></slot>
              <slot name="description"></slot>
              <slot></slot>
        </section>   
    `;
  }
}
window.customElements.define('website-section', WebsiteSection);
