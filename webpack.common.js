const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const ImageMinimizerPlugin = require('image-minimizer-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  entry: {
    index: './src/scripts/main.js',
    another: './src/scripts/email-validator.js',
    next: './src/scripts/website-section.js',
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        { from: 'src/assets/images', to: 'assets/images' },
      ],
    }), new HtmlWebpackPlugin({
      title: 'Production',
      filename: 'index.html',
      template: 'src/index.html',
    }), new MiniCssExtractPlugin(),
  ],
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.(jpe?g|png)$/i,
        type: 'asset',
      }, {
        test: /\.css$/,
        use: [ MiniCssExtractPlugin.loader, 'css-loader' ],
      },
    ],
  },
  // optimization: {
  //   minimizer: [
  //     new ImageMinimizerPlugin({
  //       minimizer: {
  //         implementation: ImageMinimizerPlugin.sharpMinify,
  //         options: {
  //           encodeOptions: {
  //             jpeg: {
  //               // https://sharp.pixelplumbing.com/api-output#jpeg
  //               quality: 100,
  //             },
  //             webp: {
  //               // https://sharp.pixelplumbing.com/api-output#webp
  //               lossless: true,
  //             },
  //             avif: {
  //               // https://sharp.pixelplumbing.com/api-output#avif
  //               lossless: true,
  //             },
  //
  //             // png by default sets the quality to 100%, which is same as lossless
  //             // https://sharp.pixelplumbing.com/api-output#png
  //             png: {},
  //
  //             // gif does not support lossless compression at all
  //             // https://sharp.pixelplumbing.com/api-output#gif
  //             gif: {},
  //           },
  //         },
  //       },
  //     }),
  //   ],
  // },
};
