import {enableButton, makeUnhidden} from "./utils";

function post(){
    fetch("http://localhost:3000/analytics/user", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify([])
    }).then((r) => {
        if (r.ok) {
            return r.json();
        }
        return Promise.reject(r);
    }).then((r) => {
    }).catch((error) => {
        console.log(error.statusText)
    })
}

onmessage = (e) => {
    if (e.data.action === "send"){
        post();
    }
}
